using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Doors: MonoBehaviour
{
    public AudioSource asource;
    public int sceneId;

    private void Start()
    {
        if (PlayerPrefs.HasKey("doorSceneId")) sceneId = PlayerPrefs.GetInt("doorSceneId");
    }
    private void OnMouseDown()
    {
        StartCoroutine(OpenDoors());
    }

    IEnumerator OpenDoors()
    {
        asource.Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneId);
    }
}
