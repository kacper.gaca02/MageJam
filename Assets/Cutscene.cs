using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Cutscene : MonoBehaviour
{
    public SpriteRenderer blackSegment;
    private float _fadeOutTime = 2f;

    private void Start()
    {
        StartCoroutine(FadeTo(Color.clear));
        PlayerPrefs.SetInt("currentLevel", 0);
        PlayerPrefs.SetInt("doorSceneId", 2);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StopAllCoroutines();
            StartCoroutine(FadeThroughBlack());
        }
    }

    private IEnumerator FadeThroughBlack()
    {
        yield return FadeTo(Color.black);
        yield return FadeTo(Color.clear);
    }

    private IEnumerator FadeTo(Color destination)
    {
        Color originalColor = blackSegment.color;

        for (float t = 0.01f; t < _fadeOutTime; t += Time.deltaTime)
        {
            blackSegment.color = Color.Lerp(originalColor, destination, t / _fadeOutTime);
            yield return null;
        }

        blackSegment.color = destination;
    }
}
