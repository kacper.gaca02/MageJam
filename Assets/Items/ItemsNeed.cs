using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsNeed : MonoBehaviour
{
    public Item itemNeeded;
    public Items items;

    // Start is called before the first frame update
    private void OnMouseDown()
    {
        if (items.equipItem == itemNeeded && items.ItemHeld == true)
        {
            items.equipItem = Item.Key;
        }
    }
}
