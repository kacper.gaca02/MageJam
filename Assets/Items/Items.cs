using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    public Item equipItem;
    public bool ItemHeld;
    public Sprite[] AllItems;
    private void OnMouseDown()
    {
        if (!ItemHeld)
        {
            ItemHeld = true;
        }
        else
        {
            ItemHeld = false;
        }
    }
    void Update()
    {
        this.GetComponent<SpriteRenderer>().sprite = AllItems[(int)equipItem];
    }
}

public enum Item
{
    None,
    Rake,
    Key,
    Bone,
    Stick
}