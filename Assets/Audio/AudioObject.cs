using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : MonoBehaviour
{
    public static AudioObject instance;
    public AudioClip secondClip;

    private void Awake()
    {
        if (instance != null) Destroy(gameObject);
        else instance = this; DontDestroyOnLoad(this.gameObject);
    }

}
