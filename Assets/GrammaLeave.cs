using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrammaLeave : MonoBehaviour
{
    public Jumpscare _jumpscare;
    void Update()
    {
        this.GetComponent<BoxCollider2D>().enabled = !_jumpscare._figAttack;
        this.GetComponentInChildren<SpriteRenderer>().enabled = !_jumpscare._figAttack;
    }
    void OnMouseDown()
    {
        _jumpscare.JumpScare();
    }
}

