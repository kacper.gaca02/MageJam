using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jumpscare : MonoBehaviour
{
    public AudioSource audio;
    public SpriteRenderer renderer;
    public GameObject render;
    private int _lifes;
    public List<string> ListOfLetters;
    public Sprite[] Keys;
    private string _currentLetter;
    public GameObject KeyTarget;
    [HideInInspector] public bool _figAttack;
    public GameObject[] Figs;
    public GameObject[] Sides;
    private int _figsDefeated = 0;
    private float TimeToBite = 2f;
    private bool _nomActive;
    public Sprite[] HpState;
    public GameObject HpBar;
    public Items items;
    [HideInInspector] bool BossFight;
    private string _currentLetter2;
    public GameObject KeyTarget2;
    private string _currentLetter3;
    public GameObject KeyTarget3;
    private void Start()
    {
        _lifes = PlayerPrefs.GetInt("Lifes", 3);
        HpBar.GetComponent<SpriteRenderer>().sprite = HpState[_lifes];
        KeyTarget.SetActive(false);
        int biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
        _currentLetter = ListOfLetters[biggie%ListOfLetters.Count];
        KeyTarget.GetComponent<SpriteRenderer>().sprite = Keys[biggie%ListOfLetters.Count];
        biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
        _currentLetter2 = ListOfLetters[biggie%ListOfLetters.Count];
        biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
        _currentLetter3 = ListOfLetters[biggie%ListOfLetters.Count];
        _figAttack = true;
        Invoke("AttackingFigs", 2f);
    }
    void Update()
    {
        if (items.equipItem == Item.Key)
        {
            BossFight = true;
        }
        if (!_nomActive)
        {
           renderer.gameObject.SetActive(false);
        }
        if (_figAttack)
        {
            foreach (char c in Input.inputString)
            {
                if (_currentLetter == c.ToString().ToLower() && _figsDefeated < 3)
                {
                    StopAllCoroutines();//In this Script
                    //QTE succesfull
                    KeyTarget.SetActive(false);
                    Figs[_figsDefeated].SetActive(false);
                    //Try to spawn another soon with new QTE
                    Invoke("AttackingFigs", 1.5f);
                    int biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
                    _currentLetter = ListOfLetters[biggie%ListOfLetters.Count];
                    KeyTarget.GetComponent<SpriteRenderer>().sprite = Keys[biggie%ListOfLetters.Count];
                    _figsDefeated++;
                }
                if (_currentLetter2 == c.ToString().ToLower() || _currentLetter3 == c.ToString().ToLower() && BossFight)
                {
                    StopAllCoroutines();
                    if (_currentLetter2 == c.ToString().ToLower()) KeyTarget2.SetActive(false);
                    if (_currentLetter3 == c.ToString().ToLower()) KeyTarget3.SetActive(false);
                    if (!KeyTarget2.activeInHierarchy && !KeyTarget3.activeInHierarchy)
                    {
                        PlayerPrefs.SetInt("ending", 1);
                        
                    }
                }
            }
        }
    }
    public void AttackingFigs()
    {
        if (_figsDefeated < 2){
            _figAttack = true;
            Figs[_figsDefeated].SetActive(true);
            Figs[_figsDefeated].GetComponent<AudioSource>().Play();
            Sides[_figsDefeated].SetActive(false);
            StartCoroutine("Nom");
            KeyTarget.SetActive(true);
        } else {
            _figAttack = false;
        }
    }
    public void JumpScare()
    {
        if (items.equipItem == Item.Key)
        {
            SceneManager.LoadScene("Ending");
            BossFight = true;
            audio.Play();
            render.SetActive(true);
            StartCoroutine("Nom");
        } else {
            audio.Play();
            renderer.gameObject.SetActive(true);
            _nomActive = true;
            StartCoroutine("Nom");
        }
    }
    IEnumerator Nom()
    {
        yield return new WaitForSeconds(TimeToBite);
        _lifes--;
        HpBar.GetComponent<SpriteRenderer>().sprite = HpState[_lifes];
        _nomActive = false;
        yield return new WaitForSeconds(0.01f);
    }
}