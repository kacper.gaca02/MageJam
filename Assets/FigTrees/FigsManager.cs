using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FigsManager : MonoBehaviour
{
    public AudioClip myClip;
    private float _reduce = 2.5f;
    public AudioSource leavesSound;
    public EndOfTheDay eotd;
    public GameObject[] figOnTrees = new GameObject[6];
    public Sprite[] figSprites;
    private int _figTreeId = 0;
    [SerializeField] private int _figOnTree = 4;
    public GameObject player;
    public GameObject KeyTarget;
    public Sprite[] Keys;
    public Dialogue dialogue;
    public List<string> ListOfLetters = new List<string> {"a","s","d","f"};
    private string _currentFig;
    public int currentLevel = 0;
    private int _currentLife;
    private bool _penalty = false;
    private void Awake()
    {
        //initial position
        Vector3 stFigPos = figOnTrees[0].transform.position;
        player.transform.position = new Vector3(stFigPos.x-(stFigPos.x/_reduce),stFigPos.y,0);
        //generate new key
        int biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
        _currentFig = ListOfLetters[biggie%ListOfLetters.Count];
        KeyTarget.GetComponent<SpriteRenderer>().sprite = Keys[biggie%ListOfLetters.Count];
        KeyTarget.SetActive(true);
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("currentLevel")) currentLevel = PlayerPrefs.GetInt("currentLevel");
        else currentLevel = 0;
    }

    void Update()
    {
        //if(Input.GetKeyDown(KeyCode.Space)) SuccessfullGather();
        if (!_penalty){
            foreach (char c in Input.inputString)
            {
                if (_currentFig == c.ToString().ToLower())
                {
                    SuccessfullGather();
                }
                else
                {
                    switch (currentLevel){
                        case 0:
                            break;
                        case 1:
                            StartCoroutine("WaitAMoment", 0.25f);
                            break;
                        case 2:
                            StartCoroutine("WaitAMoment", 1f);
                            break;
                    }
                }
            }
        }
    }
    public void SuccessfullGather()
    {
        //play sound
        leavesSound.Play();
        //change location
        _figOnTree--;
        figOnTrees[_figTreeId].GetComponent<SpriteRenderer>().sprite = figSprites[_figOnTree];
        //generate new letter
        _currentFig = "";
        int biggie = UnityEngine.Random.Range(0,ListOfLetters.Count*10);
        _currentFig = ListOfLetters[biggie%ListOfLetters.Count];
        StartCoroutine("KeyChange", biggie%ListOfLetters.Count);
        //go to next tree
        if(_figOnTree == 0) GoToNextFig();
   }
/*
    public void FailedGather()
    {

    }
*/
    public void GoToNextFig()
    {
        if (_figTreeId < figOnTrees.Length-1)
        {
            if (currentLevel == 1 && _figTreeId == 1) 
            {
              AudioSource src = AudioObject.instance.GetComponent<AudioSource>();
              src.Stop(); PlayerPrefs.SetInt("doorSceneId", 3); 
              src.clip = myClip;
              src.Play();
            }
            _figTreeId++;
            _figOnTree = 4;
            Vector3 figPos = figOnTrees[_figTreeId].transform.position;
            player.transform.position = new Vector3(figPos.x-(figPos.x/_reduce), figPos.y, 0);
        }
        else
        {
            KeyTarget.SetActive(false);
            eotd.EndDay();
            PlayerPrefs.SetInt("currentLevel", currentLevel);
        }
        if (_figTreeId == 3 || _figTreeId == 4 || _figTreeId == 5) player.GetComponent<SpriteRenderer>().flipX = true;
    }
    IEnumerator WaitAMoment(float amount)
    {   
        _penalty = true;
        yield return new WaitForSeconds(amount);
        _penalty = false;
    }
    IEnumerator KeyChange(int num)
    {
        KeyTarget.GetComponent<SpriteRenderer>().sprite = Keys[4];
        yield return new WaitForSeconds(0.5f);
        KeyTarget.GetComponent<SpriteRenderer>().sprite = Keys[num];
    }
}
