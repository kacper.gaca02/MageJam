using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TheDayRooms : MonoBehaviour
{
    public AudioSource audio;
    public int sceneId;
    private int _lifes;
    void Start()
    {
        _lifes = PlayerPrefs.GetInt("Lifes", 3);
    }
    private void OnMouseDown()
    {
        StartCoroutine(GoToDirection());
    }

    IEnumerator GoToDirection()
    {
        PlayerPrefs.SetInt("Lifes",_lifes);
        audio.Play();
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneId);
    }
}
