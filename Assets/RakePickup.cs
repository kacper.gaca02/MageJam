using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RakePickup : MonoBehaviour
{
    public Items items;
    void OnMouseDown()
    {
        items.equipItem = Item.Rake;
    }
}
