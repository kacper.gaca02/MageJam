using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour
{
    public string[] getFrom;
    public int dialogueId;
    private Text dial;
    private int ending;
    private void Awake()
    {
        ending = PlayerPrefs.GetInt("ending", 0);
        dial = GetComponent<Text>();
    }

    private void Start()
    {
        StartCoroutine(AnimateText());
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(dialogueId == getFrom.Length-1) SceneManager.LoadScene(1);
            dialogueId++;
            StopAllCoroutines();
            dial.text = "";
            StartCoroutine(AnimateText());
        }
    }

    IEnumerator AnimateText()
    {
        string gettingFrom = getFrom[dialogueId];
        for (int i = 0; i < gettingFrom.Length; i++)
        {
            dial.text += gettingFrom[i];
            yield return new WaitForSeconds(.05f);
        }
    }

}
