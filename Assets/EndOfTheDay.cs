using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndOfTheDay : MonoBehaviour
{
    public FigsManager figsManager;
    Image img;
    public Text txt;
    public float duration;
    private void Awake()
    {
        img = GetComponent<Image>();
    }


    public void EndDay()
    {
        StartCoroutine(FadeTo(Color.black));
        figsManager.currentLevel++;
    }

    private IEnumerator FadeTo(Color destination)
    {
        Color originalColor = img.color;

        for (float t = 0.01f; t < duration; t += Time.deltaTime)
        {
            img.color = Color.Lerp(originalColor, destination, t / duration);
            yield return null;
        }

        img.color = destination;
        txt.text = "DAY "+(figsManager.currentLevel+1);
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }
}
