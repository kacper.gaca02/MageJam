using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeysPressDetector : MonoBehaviour
{
    public List<string> _currentTree = new List<string> {"f","s","a","d"};
    void Update()
    {
        foreach (char c in Input.inputString)
        {
            for (int i = 0; i < _currentTree.Count; i++)
            {
                if (_currentTree[i] == c.ToString().ToLower()){
                    print("Yup");
                    _currentTree.RemoveAt(i);
                }
                else
                {
                    print("Nope");
                }
            }
        }
    }
}
